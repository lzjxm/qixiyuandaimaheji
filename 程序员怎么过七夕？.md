不少人对程序员刻板的印象常常是性格木讷、不善交际、不解风情等，仿佛就如他们整天打交道的机器一般精准而冷漠。  

但程序员也是人啊，也有温柔浪漫的一面。今天是七夕，咱就来看看程序员都是怎么表达爱意的。   
   
1. 前谷歌工程师 Colin McMillen 用一段 perl 代码向他的女友 Kristen Stubbs 求婚：

```  
#!/usr/bin/perl -w
use strict;

     my$f=           $[;my
   $ch=0;sub       l{length}
 sub r{join"",   reverse split
("",$_[$[])}sub ss{substr($_[0]
,$_[1],$_[2])}sub be{$_=$_[0];p
 (ss($_,$f,1));$f+=l()/2;$f%=l 
  ();$f++if$ch%2;$ch++}my$q=r
   ("\ntfgpfdfal,thg?bngbj".    
    "naxfcixz");$_=$q; $q=~
      tr/f[a-z]/ [l-za-k] 
        /;my@ever=1..&l
          ;my$mine=$q
            ;sub p{
             print
              @_;
               }

       be $mine for @ever
```  
这个代码是可以执行的，会输出一行：

```
kristen, will you marry me?
```
而你如果搜索上述两个名字，就会发现他们其实共同发表过好几篇论文：  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184002_9c2143ad_1899542.jpeg "1.jpg")    
（来源：https://www.perlmonks.org/index.pl?node_id=384100）  
      
2. 在网页上发布以下文字：   

```
99669999996669999996699666699666999966699666699
99699999999699999999699666699669966996699666699
99669999999999999996699666699699666699699666699
99666699999999999966666999966699666699699666699
99666666999999996666666699666699666699699666699
99666666669999666666666699666669966996699666699
99666666666996666666666699666666999966669999996
```
然后按  **CTRL+F** （搜索，手机上可选『搜索页面内容』），输入数字  **9** ，你就会看到不一样的内容：  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184141_21a53252_1899542.jpeg "2.jpg")  
（来自网络）    
  
3. 美国的一位工程师（严格来说这位不是程序员）Ben Kokes亲手为女友打造了一枚“LED发光戒指”。当他靠近女友时，女友手上的这枚的戒指就会发光。（袖子里藏了一个交变磁场发生器，而且得非常近）  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184332_919c32dd_1899542.jpeg "3.jpg")  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184343_650336eb_1899542.jpeg "4.jpg")   
（来源：http://www.kokes.net/projectlonghaul/projectlonghaul.htm）  
  
4.抖音表白神器：  
![输入图片说明](https://gitee.com/crossin/snippet/raw/master/love/love.gif "在这里输入图片标题")     
来自 @小灰辉：[送你个情人节礼物：抖音同款表白神器](http://https://mp.weixin.qq.com/s?__biz=MjM5MDEyMDk4Mw==&mid=2650168159&idx=1&sn=f6523ac435295d9ae2e748f148e14761&chksm=be4b5627893cdf318723718640bc31f32f8a3b03098b3d3b9b296eecb138c6babde361094910&scene=21#wechat_redirect)  
   
5. 照片组字  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184624_18c950c7_1899542.jpeg "6.jpg")   
这本是一个获取微信好友头像，组成文字的程序。但换成对方的照片拿来表白，应该也是可以吧~（上图是我今天修改代码后发的朋友圈）
方法来自 @TED：[用Python写一份独特的元宵节祝福](http://https://mp.weixin.qq.com/s?__biz=MjM5MDEyMDk4Mw==&mid=2650168172&idx=1&sn=a88713afe389e11701bb6f3c60b21f38&chksm=be4b5614893cdf02fcdd2aa3928e5a8f2a2d9edf6fc1c67e42a3981f9eadfff5e11a4f121707&scene=21#wechat_redirect)  
  
我自己也写过的一些小代码：  
  
6. 玫瑰曲线  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184728_0c894f95_1899542.jpeg "7.jpg")  

```
import math
rad = 12
heart = '♥'
curve = []
for i in range(rad*2+1):
    curve.append([])
    for j in range(rad*2+1):
        curve[i].append(' ')
for n in range(1,100):
    print(n)
    for k in range(360):
        angle = k * math.pi / 180
        x = int(rad * math.sin(n * angle) * math.sin(angle)) + rad
        y = int(rad * math.sin(n * angle) * math.cos(angle)) + rad
        curve[x][y] = heart
    for i in range(rad*2+1):
        for j in range(rad*2+1):
            print(curve[i][j], end=' ')
            curve[i][j] = ' '
        print()
```
这段代码可以画出99朵『玫瑰曲线』。  

（来源：https://www.zhihu.com/question/274711258/answer/410957267）  
  
7. 由0和1组成的爱心图案（也有人说像屁股……）
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184848_5ce436f9_1899542.gif "640.gif")  
这是我早些年用 FLASH 的 AS 脚本写的一段代码。灵感来源自当时人人网上很火的一个项目：  
一个程序员为庆祝和女友相恋一周年制作的网页  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0807/184950_54c331af_1899542.jpeg "9.jpg")  
  
然而，当我这次想起来，再去搜索时，发现网站的地址已经从 
love.hackerzhou.me    
变成了    
hackerzhou.me/ex_love    
当时的主人公也早已劳燕分飞    
 

> 谁还记得，是谁先说永远的爱我？

> 以前的一句话是我们以后的伤口。

> 过了太久没人记得，当初那些温柔。

> 我和你手牵手，说要一起走到最后。

> 《记得》
  
所以嘛，浪漫的表白固然让人心情澎湃，但其实也并没有什么乱用。对的人在一起，每天都是情人节；不对的人，再浪漫的表白，也最多换来个『十分感动，然后拒绝了他』。  

   







