不知不觉七夕节又双叒叕来了，在这个一年n度的虐狗节，小编整理了一波gitee上的开源表白代码作品分享给单身汪们参考学习，祝大家能早日脱单哟！

PS:以下为[「表白源代码合集.md」](https://gitee.com/gitee-community/qixiyuandaimaheji/blob/master/%E8%A1%A8%E7%99%BD%E6%BA%90%E4%BB%A3%E7%A0%81%E5%90%88%E9%9B%86.md)的部分项目，大家可以点击查看&下载，欢迎大家继续投递补充～
  
      
 **作品1：小栋栋机器人，七夕给女神做的聊天机器人APP**     
![输入图片说明](https://oscimg.oschina.net/oscnet/709174d69c7d8e3eacebc5aa1c0cd1055ac.jpg "在这里输入图片标题")     
无论回什么都会马上回复，语气还超级温柔，这样还要男盆友干什么！！！       
项目地址：https://gitee.com/SuDong007_admin/small_robot 
  
    
 **作品2:仿抖音同款表白小程序，在 Python让你的情人节更完美**   
![输入图片说明](https://gitee.com/crossin/snippet/raw/master/love/love.gif "在这里输入图片标题")  
 项目地址：https://gitee.com/crossin/snippet/tree/master/love       
    
 **作品3:情人节烟花**   
      
![输入图片说明](https://oscimg.oschina.net/oscnet/93ab97458b7ccf1a87d4915b22a4ea34935.jpg "烟花.gif")     
一起来放烟花吧！        
项目地址：https://gitee.com/huachuan005/yanhua      
 

 **作品4:HTML+CSS3+JS+JQuery实现七夕言情动画**    
        
![输入图片说明](https://oscimg.oschina.net/oscnet/9b3416c0093f1536359d88da502630da7af.jpg "在这里输入图片标题")      
项目地址：https://gitee.com/mangochild/qixianimate      
        
  
 **作品5：阿狸桃子爱情表白源码**     
  
![输入图片说明](https://oscimg.oschina.net/oscnet/b7f4379e42b1f09992723e74b2741e62fd9.jpg "在这里输入图片标题")     
项目地址：https://gitee.com/yanhom1314/biaobai    
  
  
 **作品6：12款表白网页源码**    
     
![输入图片说明](https://oscimg.oschina.net/oscnet/a493dce274e1b6fe65e05b3e723918b5722.jpg "在这里输入图片标题")    
被花里胡哨的特效闪瞎QAQ      
项目地址：https://gitee.com/everbest_admin/LoveSourceCode  

 **作品7、爱情表白纪念**   
  
![输入图片说明](https://oscimg.oschina.net/oscnet/7d5acd672f552657368dcf12f91c8e77f9c.jpg "在这里输入图片标题")    
项目地址：https://gitee.com/liuchen150/liuchen    

 **作品8、情侣空间**     
   
![输入图片说明](https://oscimg.oschina.net/oscnet/22e1bed1130563ae91071794bce002ac9fc.jpg "在这里输入图片标题")    
项目地址：https://gitee.com/everbest_admin/LoveSourceCode  